/* blasMatrixTest.cpp ---
 * 
 * Author: Julien Wintz
 * Created: Mon Jul 15 16:34:45 2013 (+0200)
 * Version: 
 * Last-Updated: Fri Jul 19 15:28:40 2013 (+0200)
 *           By: Thibaud Kloczko
 *     Update #: 175
 */

/* Change Log:
 * 
 */

#include "blasMatrixTest.h"
#include "blasMatrixTestConfig.h"

#include <dtkMath>

void blasMatrixTestCase::initTestCase(void)
{
    dtkMath::matrix::pluginManager().initialize(BLAS_MATRIX_TEST_PLUG_PATH);
}

void blasMatrixTestCase::init(void)
{

}

// void blasMatrixTestCase::testMatrixOperatorsInt(void)
// {
//     dtkMatrixTestCase::testMatrixOperators("blasMatrixInt");
// }

void blasMatrixTestCase::testMatrixOperatorsFloat(void)
{
    dtkMatrixTestCase::testMatrixOperators("blasMatrixFloat");
}

void blasMatrixTestCase::testMatrixOperatorsDouble(void)
{
    dtkMatrixTestCase::testMatrixOperators("blasMatrixDouble");
}

void blasMatrixTestCase::benchMatrixOperatorsDouble(void)
{
    dtkMatrixTestCase::benchMatrix("blasMatrixDouble", 3000, 256);
}

void blasMatrixTestCase::cleanupTestCase(void)
{

}

void blasMatrixTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(blasMatrixTest, blasMatrixTestCase)
