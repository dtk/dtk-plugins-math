### CMakeLists.txt ---
## 
## Author: Julien Wintz
## Created: Wed Jul 17 14:03:36 2013 (+0200)
## Version: 
## Last-Updated: Thu Jul 18 18:06:10 2013 (+0200)
##           By: Julien Wintz
##     Update #: 11
######################################################################
## 
### Change Log:
## 
######################################################################

add_subdirectory(dtkMatrix)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/dtkMatrix)

if(BUILD_BLAZE_MATRIX_PLUGIN)
  add_subdirectory(blazeMatrix)
endif(BUILD_BLAZE_MATRIX_PLUGIN)

if(BUILD_EIGEN_MATRIX_PLUGIN)
  add_subdirectory(eigenMatrix)
endif(BUILD_EIGEN_MATRIX_PLUGIN)

if(BUILD_BLAS_MATRIX_PLUGIN)
  add_subdirectory(blasMatrix)
endif(BUILD_BLAS_MATRIX_PLUGIN)

if(BUILD_BLAS_MATRIX_PLUGIN AND BUILD_BLAZE_MATRIX_PLUGIN AND BUILD_EIGEN_MATRIX_PLUGIN)
  add_subdirectory(allMatrix)
endif(BUILD_BLAS_MATRIX_PLUGIN AND BUILD_BLAZE_MATRIX_PLUGIN AND BUILD_EIGEN_MATRIX_PLUGIN)
