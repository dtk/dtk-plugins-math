/* dtkMatrixTest.h ---
 * 
 * Author: Julien Wintz
 * Created: Mon Jul 15 16:34:03 2013 (+0200)
 * Version: 
 * Last-Updated: jeu. juil. 18 09:22:33 2013 (+0200)
 *           By: Nicolas Niclausse
 *     Update #: 23
 */

/* Change Log:
 * 
 */

#pragma once

#include <dtkTest>

class dtkMatrixTestCase : public QObject
{
    Q_OBJECT

protected slots:
    virtual void testMatrixOperators(const QString& implementation);
    virtual void benchMatrix(const QString& implementation, int loop, int size);
};
