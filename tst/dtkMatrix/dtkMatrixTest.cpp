/* dtkMatrixTest.cpp ---
 * 
 * Author: Julien Wintz
 * Created: Mon Jul 15 16:34:45 2013 (+0200)
 * Version: 
 * Last-Updated: Fri Jul 19 10:11:53 2013 (+0200)
 *           By: Thibaud Kloczko
 *     Update #: 398
 */

/* Change Log:
 * 
 */

#include "dtkMatrixTest.h"

#include <dtkMath>

void randomInit(dtkMatrix& mat)
{
    for (int i = 0; i < mat.rowCount(); ++i) {
	for (int j = 0; j < mat.colCount(); ++j) {
	    mat.setAt(i, j, QVariant(rand() / (double)RAND_MAX));
	}
    }
}

void dtkMatrixTestCase::benchMatrix(const QString& implementation, int loop, int size)
{
    srand(2006);
    dtkMatrix *m1 = dtkMath::matrix::pluginFactory().create(implementation);
    dtkMatrix *m2 = dtkMath::matrix::pluginFactory().create(implementation);
    dtkMatrix *m3 = dtkMath::matrix::pluginFactory().create(implementation);
    m1->allocate(size, size);
    m2->allocate(size, size);
    randomInit(*m1);
    randomInit(*m2);

    QTime time;
    time.start();
    for (int i =0; i < loop; ++i) {
	*m3 = *m1 * *m2;
    }
    qDebug() << "elapsed:" << time.elapsed()/1000.0 << "sec";
}

void dtkMatrixTestCase::testMatrixOperators(const QString& implementation)
{
    dtkMatrix *m1 = dtkMath::matrix::pluginFactory().create(implementation);

    Q_ASSERT_X(m1, Q_FUNC_INFO, qPrintable("Unable to create " + implementation));

    m1->allocate(2, 2);
    m1->setAt(0, 0, 3);
    m1->setAt(1, 0, 2);
    m1->setAt(0, 1, -1);
    m1->setAt(1, 1, 1);

    dtkMatrix *m2 = dtkMath::matrix::pluginFactory().create(implementation);

    Q_ASSERT_X(m2, Q_FUNC_INFO, qPrintable("Unable to create " + implementation));

    m2->allocate(2, 2);
    m2->setAt(0, 0, 1);
    m2->setAt(1, 0, 0);
    m2->setAt(0, 1, 0);
    m2->setAt(1, 1, 1);

    dtkMatrix *m3 = dtkMath::matrix::pluginFactory().create(implementation);

    Q_ASSERT_X(m3, Q_FUNC_INFO, qPrintable("Unable to create " + implementation));

    m3->allocate(2, 2);
    m3->setAt(0, 0, 1);
    m3->setAt(1, 0, -1);
    m3->setAt(0, 1, 2);
    m3->setAt(1, 1, 1);

    dtkMatrix *m4 = dtkMath::matrix::pluginFactory().create(implementation);

    Q_ASSERT_X(m4, Q_FUNC_INFO, qPrintable("Unable to create " + implementation));

    *m4 = *m1 + *m2;
    *m4 = *m1 - *m2;
    *m4 = *m1 * *m3;

    QVERIFY2(m4->at(0, 0) == QVariant::fromValue(4), qPrintable("for implementation " + implementation));
    QVERIFY2(m4->at(0, 1) == QVariant::fromValue(5), qPrintable("for implementation " + implementation));
    QVERIFY2(m4->at(1, 0) == QVariant::fromValue(1), qPrintable("for implementation " + implementation));
    QVERIFY2(m4->at(1, 1) == QVariant::fromValue(5), qPrintable("for implementation " + implementation));

    delete m1;
    delete m2;
    delete m3;
    delete m4;
}
