/* blazeMatrixTest.h ---
 * 
 * Author: Julien Wintz
 * Created: Mon Jul 15 16:34:03 2013 (+0200)
 * Version: 
 * Last-Updated: Thu Jul 18 16:38:48 2013 (+0200)
 *           By: Thibaud Kloczko
 *     Update #: 19
 */

/* Change Log:
 * 
 */

#pragma once

#include <dtkMatrixTest.h>

class blazeMatrixTestCase : public dtkMatrixTestCase
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testMatrixOperatorsInt(void);
    void testMatrixOperatorsFloat(void);
    void testMatrixOperatorsDouble(void);
    void benchMatrixOperatorsDouble(void);

private slots:
    virtual void cleanupTestCase(void);
    virtual void cleanup(void);
};
