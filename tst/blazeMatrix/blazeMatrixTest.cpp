/* blazeMatrixTest.cpp ---
 * 
 * Author: Julien Wintz
 * Created: Mon Jul 15 16:34:45 2013 (+0200)
 * Version: 
 * Last-Updated: Fri Jul 19 15:29:22 2013 (+0200)
 *           By: Thibaud Kloczko
 *     Update #: 167
 */

/* Change Log:
 * 
 */

#include "blazeMatrixTest.h"
#include "blazeMatrixTestConfig.h"

#include <dtkMath>

void blazeMatrixTestCase::initTestCase(void)
{
    dtkMath::matrix::pluginManager().initialize(BLAZE_MATRIX_TEST_PLUG_PATH);
}

void blazeMatrixTestCase::init(void)
{

}

void blazeMatrixTestCase::testMatrixOperatorsInt(void)
{
    dtkMatrixTestCase::testMatrixOperators("blazeMatrixInt");
}

void blazeMatrixTestCase::testMatrixOperatorsFloat(void)
{
    dtkMatrixTestCase::testMatrixOperators("blazeMatrixFloat");
}

void blazeMatrixTestCase::testMatrixOperatorsDouble(void)
{
    dtkMatrixTestCase::testMatrixOperators("blazeMatrixDouble");
}

void blazeMatrixTestCase::benchMatrixOperatorsDouble(void)
{
    dtkMatrixTestCase::benchMatrix("blazeMatrixDouble", 3000, 256);
}

void blazeMatrixTestCase::cleanupTestCase(void)
{

}

void blazeMatrixTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(blazeMatrixTest, blazeMatrixTestCase)
