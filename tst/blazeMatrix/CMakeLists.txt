### CMakeLists.txt ---
## 
## Author: Thibaud Kloczko
## Created: Tue Jul 16 09:23:05 2013 (+0200)
######################################################################
## 
### Change Log:
## 
######################################################################

project(blazeMatrixPluginTest)

## ###################################################################
## Build tree setup
## ###################################################################

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

## ###################################################################
## 
## ###################################################################

configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/blazeMatrixTestConfig.h.in"
  "${CMAKE_CURRENT_SOURCE_DIR}/blazeMatrixTestConfig.h")

## ###################################################################

set(${PROJECT_NAME}_HEADERS
  blazeMatrixTest.h)

set(${PROJECT_NAME}_SOURCES
  blazeMatrixTest.cpp)

## ###################################################################
## Input - introspected
## ###################################################################

create_test_sourcelist(
    ${PROJECT_NAME}_SOURCES_TST
    ${PROJECT_NAME}.cpp
  ${${PROJECT_NAME}_SOURCES})

## ###################################################################
## Build rules
## ###################################################################

include_directories(${CMAKE_SOURCE_DIR}/tst/dtkMatrix)

add_executable(${PROJECT_NAME}
  ${${PROJECT_NAME}_SOURCES_TST}
  ${${PROJECT_NAME}_SOURCES})

## ###################################################################
## Link rules
## ###################################################################

qt5_use_modules(${PROJECT_NAME} Core)
qt5_use_modules(${PROJECT_NAME} Test)

target_link_libraries(${PROJECT_NAME} dtkMath)
target_link_libraries(${PROJECT_NAME} dtkMatrixPluginTest)

## ###################################################################
## Test rules
## ###################################################################

add_test(blazeMatrixTest ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/blazeMatrixPluginTest blazeMatrixTest)
