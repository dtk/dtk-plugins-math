/* eigenMatrixTest.cpp ---
 * 
 * Author: Julien Wintz
 * Created: Mon Jul 15 16:34:45 2013 (+0200)
 * Version: 
 */

/* Change Log:
 * 
 */

#include "eigenMatrixTest.h"
#include "eigenMatrixTestConfig.h"

#include <dtkMath>

void eigenMatrixTestCase::initTestCase(void)
{
    dtkMath::matrix::pluginManager().initialize(EIGEN_MATRIX_TEST_PLUG_PATH);
}

void eigenMatrixTestCase::init(void)
{

}

void eigenMatrixTestCase::testMatrixOperatorsInt(void)
{
    dtkMatrixTestCase::testMatrixOperators("eigenMatrixInt");
}

void eigenMatrixTestCase::testMatrixOperatorsFloat(void)
{
    dtkMatrixTestCase::testMatrixOperators("eigenMatrixFloat");
}

void eigenMatrixTestCase::testMatrixOperatorsDouble(void)
{
    dtkMatrixTestCase::testMatrixOperators("eigenMatrixDouble");
}

void eigenMatrixTestCase::benchMatrixOperatorsDouble(void)
{
    dtkMatrixTestCase::benchMatrix("eigenMatrixDouble", 3000, 256);
}

void eigenMatrixTestCase::cleanupTestCase(void)
{

}

void eigenMatrixTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(eigenMatrixTest, eigenMatrixTestCase)
