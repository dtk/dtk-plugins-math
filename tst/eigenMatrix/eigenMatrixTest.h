/* eigenMatrixTest.h ---
 * 
 * Author: Julien Wintz
 * Created: Mon Jul 15 16:34:03 2013 (+0200)
 * Version: 
 */

/* Change Log:
 * 
 */

#pragma once

#include <dtkMatrixTest.h>

class eigenMatrixTestCase : public dtkMatrixTestCase
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testMatrixOperatorsInt(void);
    void testMatrixOperatorsFloat(void);
    void testMatrixOperatorsDouble(void);
    void benchMatrixOperatorsDouble(void);

private slots:
    virtual void cleanupTestCase(void);
    virtual void cleanup(void);
};
