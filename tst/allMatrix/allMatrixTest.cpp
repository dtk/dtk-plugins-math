/* allMatrixTest.cpp ---
 * 
 * Author: Julien Wintz
 * Created: Mon Jul 15 16:34:45 2013 (+0200)
 * Version: 
 * Last-Updated: Thu Jul 18 18:07:59 2013 (+0200)
 *           By: Julien Wintz
 *     Update #: 369
 */

/* Change Log:
 * 
 */

#include "allMatrixTest.h"
#include "allMatrixTestConfig.h"

#include <dtkMath>

void allMatrixTestCase::initTestCase(void)
{
    dtkMath::matrix::pluginManager().initialize(ALL_MATRIX_TEST_PLUG_PATH);
}

void allMatrixTestCase::init(void)
{

}

void allMatrixTestCase::testMatrixMultiplicationEigenAndBlas(void)
{
    dtkMatrix *m1 = dtkMath::matrix::pluginFactory().create("eigenMatrixDouble");

    Q_ASSERT_X(m1, Q_FUNC_INFO, qPrintable("Unable to create eigenMatrixDouble"));

    m1->allocate(2, 2);
    m1->setAt(0, 0, 3);
    m1->setAt(1, 0, 2);
    m1->setAt(0, 1, -1);
    m1->setAt(1, 1, m1->at(1, 0).value<double>() + m1->at(0, 1).value<double>());

    dtkMatrix *m2 = dtkMath::matrix::pluginFactory().create("blasMatrixDouble");

    Q_ASSERT_X(m2, Q_FUNC_INFO, qPrintable("Unable to create blasMatrixDouble"));

    m2->allocate(2, 2);
    m2->setAt(0, 0, 1);
    m2->setAt(1, 0, -1);
    m2->setAt(0, 1, 2);
    m2->setAt(1, 1, 1);

    dtkMatrix *m3 = &(*m1 * *m2);

    QVERIFY(m3->at(0, 0) == QVariant::fromValue(4));
    QVERIFY(m3->at(0, 1) == QVariant::fromValue(5));
    QVERIFY(m3->at(1, 0) == QVariant::fromValue(1));
    QVERIFY(m3->at(1, 1) == QVariant::fromValue(5));

    dtkMatrix *m4 = &(*m2 * *m1);

    QVERIFY(m4->at(0, 0) == QVariant::fromValue(7));
    QVERIFY(m4->at(0, 1) == QVariant::fromValue(1));
    QVERIFY(m4->at(1, 0) == QVariant::fromValue(-1));
    QVERIFY(m4->at(1, 1) == QVariant::fromValue(2));

    delete m1;
    delete m2;
    delete m3;
    delete m4;
}

void allMatrixTestCase::testMatrixMultiplicationEigenAndBlaze(void)
{
    dtkMatrix *m1 = dtkMath::matrix::pluginFactory().create("eigenMatrixDouble");

    Q_ASSERT_X(m1, Q_FUNC_INFO, qPrintable("Unable to create eigenMatrixDouble"));

    m1->allocate(2, 2);
    m1->setAt(0, 0, 3);
    m1->setAt(1, 0, 2);
    m1->setAt(0, 1, -1);
    m1->setAt(1, 1, m1->at(1, 0).value<double>() + m1->at(0, 1).value<double>());

    dtkMatrix *m2 = dtkMath::matrix::pluginFactory().create("blazeMatrixDouble");

    Q_ASSERT_X(m2, Q_FUNC_INFO, qPrintable("Unable to create blazeMatrixDouble"));

    m2->allocate(2, 2);
    m2->setAt(0, 0, 1);
    m2->setAt(1, 0, -1);
    m2->setAt(0, 1, 2);
    m2->setAt(1, 1, 1);

    dtkMatrix *m3 = &(*m1 * *m2);

    QVERIFY(m3->at(0, 0) == QVariant::fromValue(4));
    QVERIFY(m3->at(0, 1) == QVariant::fromValue(5));
    QVERIFY(m3->at(1, 0) == QVariant::fromValue(1));
    QVERIFY(m3->at(1, 1) == QVariant::fromValue(5));

    dtkMatrix *m4 = &(*m2 * *m1);

    QVERIFY(m4->at(0, 0) == QVariant::fromValue(7));
    QVERIFY(m4->at(0, 1) == QVariant::fromValue(1));
    QVERIFY(m4->at(1, 0) == QVariant::fromValue(-1));
    QVERIFY(m4->at(1, 1) == QVariant::fromValue(2));

    delete m1;
    delete m2;
    delete m3;
    delete m4;
}

void allMatrixTestCase::testMatrixMultiplicationBlasAndBlaze(void)
{
    dtkMatrix *m1 = dtkMath::matrix::pluginFactory().create("blasMatrixDouble");

    Q_ASSERT_X(m1, Q_FUNC_INFO, qPrintable("Unable to create blasMatrixDouble"));

    m1->allocate(2, 2);
    m1->setAt(0, 0, 3);
    m1->setAt(1, 0, 2);
    m1->setAt(0, 1, -1);
    m1->setAt(1, 1, m1->at(1, 0).value<double>() + m1->at(0, 1).value<double>());

    dtkMatrix *m2 = dtkMath::matrix::pluginFactory().create("blazeMatrixDouble");

    Q_ASSERT_X(m2, Q_FUNC_INFO, qPrintable("Unable to create blazeMatrixDouble"));

    m2->allocate(2, 2);
    m2->setAt(0, 0, 1);
    m2->setAt(1, 0, -1);
    m2->setAt(0, 1, 2);
    m2->setAt(1, 1, 1);

    dtkMatrix *m3 = &(*m1 * *m2);

    QVERIFY(m3->at(0, 0) == QVariant::fromValue(4));
    QVERIFY(m3->at(0, 1) == QVariant::fromValue(5));
    QVERIFY(m3->at(1, 0) == QVariant::fromValue(1));
    QVERIFY(m3->at(1, 1) == QVariant::fromValue(5));

    dtkMatrix *m4 = &(*m2 * *m1);

    QVERIFY(m4->at(0, 0) == QVariant::fromValue(7));
    QVERIFY(m4->at(0, 1) == QVariant::fromValue(1));
    QVERIFY(m4->at(1, 0) == QVariant::fromValue(-1));
    QVERIFY(m4->at(1, 1) == QVariant::fromValue(2));

    delete m1;
    delete m2;
    delete m3;
    delete m4;
}

void allMatrixTestCase::cleanupTestCase(void)
{

}

void allMatrixTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(allMatrixTest, allMatrixTestCase)
