/* allMatrixTest.h ---
 * 
 * Author: Julien Wintz
 * Created: Mon Jul 15 16:34:03 2013 (+0200)
 * Version: 
 * Last-Updated: Thu Jul 18 18:07:29 2013 (+0200)
 *           By: Julien Wintz
 *     Update #: 31
 */

/* Change Log:
 * 
 */

#pragma once

#include <dtkMatrixTest.h>

class allMatrixTestCase : public dtkMatrixTestCase
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testMatrixMultiplicationEigenAndBlas(void);
    void testMatrixMultiplicationEigenAndBlaze(void);
    void testMatrixMultiplicationBlasAndBlaze(void);

private slots:
    virtual void cleanupTestCase(void);
    virtual void cleanup(void);
};
