/* eigenMatrixPlugin.h ---
 * 
 * Author: Julien Wintz
 * Created: Mon Jul 15 15:47:29 2013 (+0200)
 * Version: 
 * Last-Updated: Mon Jul 15 16:24:28 2013 (+0200)
 *           By: Julien Wintz
 *     Update #: 16
 */

/* Change Log:
 * 
 */

#pragma once

#include <dtkMath>

class eigenMatrixPlugin : public dtkMatrixPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkMatrixPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.eigenMatrixPlugin" FILE "eigenMatrixPlugin.json")

public:
     eigenMatrixPlugin(void) {}
    ~eigenMatrixPlugin(void) {}

public:
    void   initialize(void);
    void uninitialize(void);
};
