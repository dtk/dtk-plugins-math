/* eigenMatrixPlugin.cpp ---
 * 
 * Author: Julien Wintz
 * Created: Mon Jul 15 15:47:19 2013 (+0200)
 * Version: 
 * Last-Updated: Tue Jul 16 12:18:33 2013 (+0200)
 *           By: Julien Wintz
 *     Update #: 23
 */

/* Change Log:
 * 
 */

#include "eigenMatrix.h"
#include "eigenMatrixPlugin.h"

#include <dtkCore>

// ///////////////////////////////////////////////////////////////////
// eigenMatrixPlugin
// ///////////////////////////////////////////////////////////////////

void eigenMatrixPlugin::initialize(void)
{
    dtkMath::matrix::pluginFactory().record("eigenMatrixInt", dtkMatrixCreator<eigenMatrixInt>);
    dtkMath::matrix::pluginFactory().record("eigenMatrixFloat", dtkMatrixCreator<eigenMatrixFloat>);
    dtkMath::matrix::pluginFactory().record("eigenMatrixDouble", dtkMatrixCreator<eigenMatrixDouble>);
}

void eigenMatrixPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(eigenMatrix)
