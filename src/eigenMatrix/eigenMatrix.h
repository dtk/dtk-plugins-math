/* eigenMatrix.h ---
 * 
 * Author: Julien Wintz
 * Created: Mon Jul 15 15:47:05 2013 (+0200)
 * Version: 
 * Last-Updated: Fri Jul 19 15:47:17 2013 (+0200)
 *           By: Thibaud Kloczko
 *     Update #: 201
 */

/* Change Log:
 * 
 */

#pragma once

#include <dtkMath>

#include <Eigen/Eigen>

template <typename T> class eigenMatrix : public dtkMatrix
{
public:
     eigenMatrix(void);
    ~eigenMatrix(void);

public:
    qulonglong colCount(void) const;
    qulonglong rowCount(void) const;

public:
    QVariant at(qulonglong i, qulonglong j) const;
    void setAt(qulonglong i, qulonglong j, QVariant value);

public:
    void allocate(qulonglong r, qulonglong c);

public:
    void *buffer(void);

public:
    void       cpy(const dtkMatrix& other);
    dtkMatrix& cln(void);
    dtkMatrix& sum(const dtkMatrix& other);
    dtkMatrix& sub(const dtkMatrix& other);
    dtkMatrix& mul(const dtkMatrix& other);

private:
    Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> m;
};

typedef eigenMatrix<int>    eigenMatrixInt;
typedef eigenMatrix<float>  eigenMatrixFloat;
typedef eigenMatrix<double> eigenMatrixDouble;

#include "eigenMatrix.tpp"
