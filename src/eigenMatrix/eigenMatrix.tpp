/* eigenMatrix.tpp ---
 * 
 * Author: Julien Wintz
 * Created: Tue Jul 16 11:38:56 2013 (+0200)
 * Version: 
 * Last-Updated: Fri Jul 19 16:29:44 2013 (+0200)
 *           By: Thibaud Kloczko
 *     Update #: 519
 */

/* Change Log:
 * 
 */

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

template <typename T> eigenMatrix<T> *eigen_cast(const dtkMatrix &other, bool& instanciated);

// ///////////////////////////////////////////////////////////////////
// eigenMatrix<T>
// ///////////////////////////////////////////////////////////////////

template <typename T> eigenMatrix<T>::eigenMatrix(void) : dtkMatrix()
{
    
}

template <typename T> eigenMatrix<T>::~eigenMatrix(void)
{
    
}

template <typename T> qulonglong eigenMatrix<T>::colCount(void) const
{
    return m.cols();
}

template <typename T> qulonglong eigenMatrix<T>::rowCount(void) const
{
    return m.rows();
}

template <typename T> QVariant eigenMatrix<T>::at(qulonglong i, qulonglong j) const
{
    return QVariant::fromValue(m(i, j));
}

template <typename T> void eigenMatrix<T>::setAt(qulonglong i, qulonglong j, QVariant value)
{
    m(i, j) = value.value<T>();
}

template <typename T> void eigenMatrix<T>::allocate(qulonglong r, qulonglong c)
{
    m.resize(r, c);
}

template <typename T> void *eigenMatrix<T>::buffer(void)
{
    return m.data();
}

template <typename T> void eigenMatrix<T>::cpy(const dtkMatrix& other)
{
    if (other.dirty) {
	m.swap(static_cast<eigenMatrix<T> *>(&(const_cast<dtkMatrix&>(other)))->m);
    } else {
	qulonglong r = other.rowCount();
	qulonglong c = other.colCount();
	m.resize(r, c);
	T *array = static_cast<T *>(const_cast<dtkMatrix&>(other).buffer());
	for(qulonglong i = 0; i < r; ++i) {
	    for(qulonglong j = 0 ; j < c; ++j) {
		m(i,j) = array[i*r+j];
	    }
	}
    }
}

template <typename T> dtkMatrix& eigenMatrix<T>::cln(void)
{
    eigenMatrix<T> *c = new eigenMatrix<T>;
    c->cpy(*this);

    return *c;
}

template <typename T> dtkMatrix& eigenMatrix<T>::sum(const dtkMatrix& other)
{
    bool instanciated = false;

    eigenMatrix<T> *rhs = eigen_cast<T>(other, instanciated);

    m += rhs->m;

    if(instanciated)
	delete rhs;

    return (*this);
}

template <typename T> dtkMatrix& eigenMatrix<T>::sub(const dtkMatrix& other)
{
    bool instanciated = false;

    eigenMatrix<T> *rhs = eigen_cast<T>(other, instanciated);

    m -= rhs->m;

    if(instanciated)
	delete rhs;

    return (*this);
}

template <typename T> dtkMatrix& eigenMatrix<T>::mul(const dtkMatrix& other)
{
    bool instanciated = false;

    eigenMatrix<T> *rhs = eigen_cast<T>(other, instanciated);

    m *= rhs->m;

    if(instanciated)
	delete rhs;

    return (*this);
}

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

template <typename T> eigenMatrix<T> *eigen_cast(const dtkMatrix &other, bool& instanciated)
{
    eigenMatrix<T> *rhs = dynamic_cast<eigenMatrix<T> *>(&const_cast<dtkMatrix &>(other));

    if(!rhs) {
        rhs = new eigenMatrix<T>;
        rhs->cpy(other);
	instanciated = true;
    }
    
    return rhs;
}
