/* blasMatrixPlugin.cpp ---
 * 
 * Author: Julien Wintz
 * Created: Mon Jul 15 15:47:19 2013 (+0200)
 * Version: 
 * Last-Updated: mar. juil. 16 15:09:28 2013 (+0200)
 *           By: Nicolas Niclausse
 *     Update #: 24
 */

/* Change Log:
 * 
 */

#include "blasMatrix.h"
#include "blasMatrixPlugin.h"

#include <dtkCore>

// ///////////////////////////////////////////////////////////////////
// blasMatrixPlugin
// ///////////////////////////////////////////////////////////////////

void blasMatrixPlugin::initialize(void)
{
    dtkMath::matrix::pluginFactory().record("blasMatrixInt", dtkMatrixCreator<blasMatrixInt>);
    dtkMath::matrix::pluginFactory().record("blasMatrixFloat", dtkMatrixCreator<blasMatrixFloat>);
    dtkMath::matrix::pluginFactory().record("blasMatrixDouble", dtkMatrixCreator<blasMatrixDouble>);
}

void blasMatrixPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(blasMatrix)
