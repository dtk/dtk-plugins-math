/* blasMatrixPlugin.h ---
 * 
 * Author: Julien Wintz
 * Created: Mon Jul 15 15:47:29 2013 (+0200)
 * Version: 
 * Last-Updated: mar. juil. 16 15:09:19 2013 (+0200)
 *           By: Nicolas Niclausse
 *     Update #: 17
 */

/* Change Log:
 * 
 */

#pragma once

#include <dtkMath>

class blasMatrixPlugin : public dtkMatrixPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkMatrixPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.blasMatrixPlugin" FILE "blasMatrixPlugin.json")

public:
     blasMatrixPlugin(void) {}
    ~blasMatrixPlugin(void) {}

public:
    void   initialize(void);
    void uninitialize(void);
};
