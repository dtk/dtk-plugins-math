/* blasMatrix.tpp ---
 * 
 * Author: Julien Wintz
 * Created: Tue Jul 16 11:38:56 2013 (+0200)
 */

/* Change Log:
 * 
 */

template <typename T> blasMatrix<T>::blasMatrix(void)
{
    d = NULL;
}

template <typename T> blasMatrix<T>::~blasMatrix(void)
{
    delete d;
}

template <typename T> qulonglong blasMatrix<T>::colCount(void) const
{
    return d->c_count;
}

template <typename T> qulonglong blasMatrix<T>::rowCount(void) const
{
    return d->r_count;
}

template <typename T> QVariant blasMatrix<T>::at(qulonglong i, qulonglong j) const
{
    return QVariant::fromValue(d->at(i, j));
}

template <typename T> void blasMatrix<T>::setAt(qulonglong i, qulonglong j, QVariant value)
{
    d->setAt(i, j, value.value<T>());
}

template <typename T> void blasMatrix<T>::allocate(qulonglong r, qulonglong c)
{
    d = new dtkMatrixPrivate<T>(r, c);
}

template <typename T> void *blasMatrix<T>::buffer(void)
{
    return d->buffer;
}

template <typename T> void blasMatrix<T>::cpy(const dtkMatrix& other)
{

    blasMatrix *matrix = static_cast<blasMatrix<T> *>(&const_cast<dtkMatrix &>(other));
    qulonglong r = matrix->rowCount();
    qulonglong c = matrix->colCount();

    if (other.dirty) {
        if (!d)
            d = new dtkMatrixPrivate<T>;
        T* buffer = d->buffer;
        d->buffer = matrix->d->buffer;
        matrix->d->buffer= buffer;
        d->r_count= r;
        d->c_count= c;
        return;
    }

    if(d) {
        delete d;
        d = NULL;
    }

    this->allocate(r, c);

    memcpy(d->buffer, matrix->d->buffer, r*c*sizeof(T));

}

template <typename T> dtkMatrix& blasMatrix<T>::cln(void)
{
    blasMatrix<T> *c = new blasMatrix<T>;
    c->cpy(*this);

    return *c;
}

template <> dtkMatrix& blasMatrix<float>::sum(const dtkMatrix& other);
template <> dtkMatrix& blasMatrix<double>::sum(const dtkMatrix& other);

template <typename T> dtkMatrix& blasMatrix<T>::sum(const dtkMatrix& other)
{
    return (*this);
}

template <> dtkMatrix& blasMatrix<float>::sub(const dtkMatrix& other);
template <> dtkMatrix& blasMatrix<double>::sub(const dtkMatrix& other);

template <typename T> dtkMatrix& blasMatrix<T>::sub(const dtkMatrix& other)
{
    return (*this);
}

template <> dtkMatrix& blasMatrix<float>::mul(const dtkMatrix& other);
template <> dtkMatrix& blasMatrix<double>::mul(const dtkMatrix& other);

template <typename T> dtkMatrix& blasMatrix<T>::mul(const dtkMatrix& other)
{
    return (*this);
}

// template <> dtkMatrix& blasMatrix<int>::mul(dtkMatrix& other)
// {
//     return other;
// }

// template <> dtkMatrix& blasMatrix<float>::mul(dtkMatrix& other)
// {
//     return other;
// }

