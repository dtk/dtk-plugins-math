/* blasMatrix.cpp ---
 * 
 * Author: Julien Wintz
 * Created: Mon Jul 15 15:46:55 2013 (+0200)
 */

/* Change Log:
 * 
 */

#include "blasMatrix.h"
#include <cblas.h>

template <> dtkMatrix& blasMatrix<float>::sum(const dtkMatrix& other)
{
    cblas_saxpy( this->d->r_count * this->d->c_count, 1.0, static_cast<float*>(const_cast<dtkMatrix &>(other).buffer()) , 1, static_cast<float*>(this->d->buffer), 1);

    return (*this);
}

template <> dtkMatrix& blasMatrix<double>::sum(const dtkMatrix& other)
{
    cblas_daxpy( this->d->r_count * this->d->c_count, 1.0, static_cast<double*>(const_cast<dtkMatrix &>(other).buffer()) , 1, static_cast<double*>(this->d->buffer), 1);

    return (*this);
}

template <> dtkMatrix& blasMatrix<float>::sub(const dtkMatrix& other)
{
    cblas_saxpy( this->d->r_count * this->d->c_count, -1.0, static_cast<float*>(const_cast<dtkMatrix &>(other).buffer()) , 1, static_cast<float*>(this->d->buffer), 1);

    return (*this);
}

template <> dtkMatrix& blasMatrix<double>::sub(const dtkMatrix& other)
{
    cblas_daxpy( this->d->r_count * this->d->c_count, -1.0, static_cast<double*>(const_cast<dtkMatrix &>(other).buffer()) , 1, static_cast<double*>(this->d->buffer), 1);

    return (*this);
}

template <> dtkMatrix& blasMatrix<float>::mul(const dtkMatrix& other)
{
    qulonglong r = this->d->r_count ;
    qulonglong c = other.colCount();
    float *buffer = new float[r*c];
    float *other_buffer = static_cast<float*>(const_cast<dtkMatrix &>(other).buffer());
    cblas_sgemm (CblasRowMajor,
		 CblasNoTrans, CblasNoTrans,r , c, this->d->c_count,
		 1.0, static_cast<float*>(this->d->buffer), r, other_buffer, other.rowCount(), 0.0, buffer, r);

    delete this->d->buffer;
    this->d->buffer = buffer;

    return (*this);
}

template <> dtkMatrix& blasMatrix<double>::mul(const dtkMatrix& other)
{
    qulonglong r = this->d->r_count ;
    qulonglong c = other.colCount();
    double *buffer = new double[r*c];
    double *other_buffer = static_cast<double*>(const_cast<dtkMatrix &>(other).buffer());
    cblas_dgemm (CblasRowMajor,
		 CblasNoTrans, CblasNoTrans,r , c, this->d->c_count,
		 1.0, static_cast<double*>(this->d->buffer), r, other_buffer, other.rowCount(), 0.0, buffer, r);

    delete this->d->buffer;
    this->d->buffer = buffer;
    return (*this);
}


