/* blasMatrix.h ---
 * 
 * Author: Julien Wintz
 * Created: Mon Jul 15 15:47:05 2013 (+0200)
 * Version: 
 * Last-Updated: Fri Jul 19 08:43:07 2013 (+0200)
 *           By: Thibaud Kloczko
 *     Update #: 173
 */

/* Change Log:
 * 
 */

#pragma once

#include <dtkMath>

template <typename T> class blasMatrix : public dtkMatrix
{

public:
     blasMatrix(void);
    ~blasMatrix(void);

public:
    qulonglong colCount(void) const;
    qulonglong rowCount(void) const;

public:
    QVariant at(qulonglong i, qulonglong j) const;
    void setAt(qulonglong i, qulonglong j, QVariant value);

public:
    void allocate(qulonglong r, qulonglong c);

public:
    void *buffer(void);

public:
    void       cpy(const dtkMatrix& other);
    dtkMatrix& cln(void);
    dtkMatrix& sum(const dtkMatrix& other);
    dtkMatrix& sub(const dtkMatrix& other);
    dtkMatrix& mul(const dtkMatrix& other);

protected:
    dtkMatrixPrivate<T> *d;
};

typedef blasMatrix<int>    blasMatrixInt;
typedef blasMatrix<float>  blasMatrixFloat;
typedef blasMatrix<double> blasMatrixDouble;

#include "blasMatrix.tpp"
