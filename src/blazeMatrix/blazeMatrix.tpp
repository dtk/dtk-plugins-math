/* blazeMatrix.tpp ---
 * 
 * Author: Thibaud Kloczko
 * Created: Tue Jul 16 15:06:03 2013 (+0200)
 */

/* Change Log:
 * 
 */

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

template <typename T> blazeMatrix<T> *blaze_cast(const dtkMatrix &other, bool& instanciated);

// ///////////////////////////////////////////////////////////////////
// blazeMatrix<T>
// ///////////////////////////////////////////////////////////////////

template <typename T> blazeMatrix<T>::blazeMatrix(void) : dtkMatrix()
{
    
}

template <typename T> blazeMatrix<T>::~blazeMatrix(void)
{

}

template <typename T> qulonglong blazeMatrix<T>::colCount(void) const
{
    return m.columns();
}

template <typename T> qulonglong blazeMatrix<T>::rowCount(void) const
{
    return m.rows();
}

template <typename T> QVariant blazeMatrix<T>::at(qulonglong i, qulonglong j) const
{
    return QVariant::fromValue(m(i,j));
}

template <typename T> void blazeMatrix<T>::setAt(qulonglong i, qulonglong j, QVariant value)
{
    m(i,j) = value.value<T>();
}

template <typename T> void blazeMatrix<T>::allocate(qulonglong r, qulonglong c)
{
    m.resize(r, c);
}

template <typename T> void *blazeMatrix<T>::buffer(void)
{
    return static_cast<void *>(m.data());
}

template <typename T> void blazeMatrix<T>::cpy(const dtkMatrix& other)
{
    if (other.dirty) {
	m.swap(static_cast<blazeMatrix<T> *>(&(const_cast<dtkMatrix&>(other)))->m);
    } else {
	qulonglong r = other.rowCount();
	qulonglong c = other.colCount();
	m.resize(r, c);
	T *array = static_cast<T *>(const_cast<dtkMatrix&>(other).buffer());
	for(qulonglong i = 0; i < r; ++i) {
	    for(qulonglong j = 0 ; j < c; ++j) {
		m(i,j) = array[i*m.spacing()+j];
	    }
	}
    }
}

template <typename T> dtkMatrix& blazeMatrix<T>::cln(void)
{
    blazeMatrix<T> *c = new blazeMatrix<T>;
    c->cpy(*this);

    return *c;
}

template <typename T> dtkMatrix& blazeMatrix<T>::sum(const dtkMatrix& other)
{
    bool instanciated = false;

    blazeMatrix<T> *rhs = blaze_cast<T>(other, instanciated);

    m += rhs->m;

    if(instanciated)
	delete rhs;

    return (*this);
}

template <typename T> dtkMatrix& blazeMatrix<T>::sub(const dtkMatrix& other)
{
    bool instanciated = false;

    blazeMatrix<T> *rhs = blaze_cast<T>(other, instanciated);

    m -= rhs->m;

    if(instanciated)
	delete rhs;

    return (*this);
}

template <typename T> dtkMatrix& blazeMatrix<T>::mul(const dtkMatrix& other)
{
    bool instanciated = false;

    blazeMatrix<T> *rhs = blaze_cast<T>(other, instanciated);

    m *= rhs->m;

    if(instanciated)
	delete rhs;

    return (*this);
}

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

template <typename T> blazeMatrix<T> *blaze_cast(const dtkMatrix &other, bool& instanciated)
{
    blazeMatrix<T> *rhs = dynamic_cast<blazeMatrix<T> *>(&const_cast<dtkMatrix &>(other));

    if(!rhs) {
        rhs = new blazeMatrix<T>;
        rhs->cpy(other);
	instanciated = true;
    }
    
    return rhs;
}
