/* blazeMatrixPlugin.h ---
 * 
 * Author: Thibaud Kloczko
 * Created: Tue Jul 16 09:08:05 2013 (+0200)
 * Version: 
 * Last-Updated: Tue Jul 16 09:08:25 2013 (+0200)
 *           By: Thibaud Kloczko
 *     Update #: 2
 */

/* Change Log:
 * 
 */

#pragma once

#include <dtkMath>

class blazeMatrixPlugin : public dtkMatrixPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkMatrixPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.blazeMatrixPlugin" FILE "blazeMatrixPlugin.json")

public:
     blazeMatrixPlugin(void) {}
    ~blazeMatrixPlugin(void) {}

public:
    void   initialize(void);
    void uninitialize(void);
};
