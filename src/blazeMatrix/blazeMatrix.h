/* blazeMatrix.h ---
 * 
 * Author: Thibaud Kloczko
 * Created: Tue Jul 16 09:07:37 2013 (+0200)
 * Version: 
 * Last-Updated: Thu Jul 18 16:29:12 2013 (+0200)
 *           By: Thibaud Kloczko
 *     Update #: 33
 */

/* Change Log:
 * 
 */

#pragma once

#include <dtkMath>
#include <blaze/Math.h>

template <typename T> class blazeMatrix : public dtkMatrix
{
public:
     blazeMatrix(void);
    ~blazeMatrix(void);

public:
    qulonglong colCount(void) const;
    qulonglong rowCount(void) const;

public:
    QVariant at(qulonglong i, qulonglong j) const;
    void setAt(qulonglong i, qulonglong j, QVariant value);

public:
    void allocate(qulonglong r, qulonglong c);

public:
    void *buffer(void);

public:
    void       cpy(const dtkMatrix& other);
    dtkMatrix& cln(void);
    dtkMatrix& sum(const dtkMatrix& other);
    dtkMatrix& sub(const dtkMatrix& other);
    dtkMatrix& mul(const dtkMatrix& other);

private:
    blaze::DynamicMatrix<T> m;
};

typedef blazeMatrix<int>    blazeMatrixInt;
typedef blazeMatrix<float>  blazeMatrixFloat;
typedef blazeMatrix<double> blazeMatrixDouble;

#include "blazeMatrix.tpp"
