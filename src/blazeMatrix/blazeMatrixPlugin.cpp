/* blazeMatrixPlugin.cpp ---
 * 
 * Author: Thibaud Kloczko
 * Created: Tue Jul 16 09:08:40 2013 (+0200)
 * Version: 
 * Last-Updated: Tue Jul 16 15:08:26 2013 (+0200)
 *           By: Thibaud Kloczko
 *     Update #: 3
 */

/* Change Log:
 * 
 */

#include "blazeMatrix.h"
#include "blazeMatrixPlugin.h"

#include <dtkCore>

// ///////////////////////////////////////////////////////////////////
// blazeMatrixPlugin
// ///////////////////////////////////////////////////////////////////

void blazeMatrixPlugin::initialize(void)
{
    dtkMath::matrix::pluginFactory().record("blazeMatrixInt", dtkMatrixCreator<blazeMatrixInt>);
    dtkMath::matrix::pluginFactory().record("blazeMatrixFloat", dtkMatrixCreator<blazeMatrixFloat>);
    dtkMath::matrix::pluginFactory().record("blazeMatrixDouble", dtkMatrixCreator<blazeMatrixDouble>);
}

void blazeMatrixPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(blazeMatrix)
